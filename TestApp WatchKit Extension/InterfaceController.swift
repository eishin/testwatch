//
//  InterfaceController.swift
//  TestApp WatchKit Extension
//
//  Created by labo2 on 2014/12/01.
//  Copyright (c) 2014年 eishin. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {
    @IBOutlet weak var Labels: WKInterfaceLabel!
    @IBOutlet weak var TimerC: WKInterfaceTimer!

    override init(context: AnyObject?) {
        // Initialize variables here.
        super.init(context: context)
        TimerC.setTextColor(UIColor.greenColor())
        Labels.setText("Counter")
        
        // Configure interface objects here.
        NSLog("%@ init", self)
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        NSLog("%@ will activate", self)
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        NSLog("%@ did deactivate", self)
        super.didDeactivate()
    }
    @IBAction func SButton() {
        Labels.setText("On")
        TimerC.start()
    }
    @IBAction func EButton() {
        Labels.setText("Off")
        TimerC.stop()
    }

}
